/*
 * Multiple Inheritance - Default methods
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public interface MI_A {
    // Default method
    default void show() {
        System.out.println("Default Method --> Multiple Inheritance A");
    }
}
