/*
 * Abstraction - Abstract class, abstract methods and regular methods
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
abstract class AnimalAbstract {
    // Abstract methods (don't have a body)
    public abstract void animalSound();
    public abstract void animalMovement();
    
    // Regular methods
    public void sleep() {
        System.out.println("The animal: sleeps");
    }
    
    public void eat() {
        System.out.println("The animal: eats");
    }
}
