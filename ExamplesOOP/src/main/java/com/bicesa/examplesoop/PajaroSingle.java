/*
 * Single Inheritance - Child Class
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public class PajaroSingle extends Pajaro {

    // Properties - Encapsulation
    private String nombre;
    private String caracter;

    // Constructor
    public PajaroSingle(String lugarNacimiento, String originarioDe, String tamanio, String[] colores, int edad, float peso, Boolean esHembra, Boolean domesticado, String nombre, String caracter) {
        super(lugarNacimiento, originarioDe, tamanio, colores, edad, peso, esHembra, domesticado);
        this.nombre = nombre;
        this.caracter = caracter;
    }
    
    // Getters and Setters - Encapsulation
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCaracter() {
        return caracter;
    }

    public void setCaracter(String caracter) {
        this.caracter = caracter;
    }

    // Methods
    public void jugar() {
        System.out.println("El pájaro juega.");
    }

    public void silbar() {
        System.out.println("El pájaro silba.");
    }

    public void hablar() {
        System.out.println("El pájaro habla.");
    }

    public void remojarse() {
        System.out.println("El pájaro se remoja.");
    }

    // Method Overriding - Runtime Polymorphism
    @Override
    public String toString() {
        return super.toString()
            + "\nPajaroSingle --> " + "nombre=" + getNombre() + ", caracter=" + getCaracter();
    }
}
