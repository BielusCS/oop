/*
 * Hierarchical Inheritance - Child Class
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public class VolksWagen extends CarHierarchical {

    // Properties - Encapsulation
    private Boolean aireAcondicionado;

    // Constructor
    public VolksWagen(String marca, String modelo, int anioLanzamiento, int potencia, int puertas, Boolean aireAcondicionado) {
        super(marca, modelo, anioLanzamiento, potencia, puertas);
        this.aireAcondicionado = aireAcondicionado;
    }

    // Getters and Setters - Encapsulation
    public Boolean getAireAcondicionado() {
        return aireAcondicionado;
    }

    public void setAireAcondicionado(Boolean aireAcondicionado) {
        this.aireAcondicionado = aireAcondicionado;
    }

    // Method Overriding - Runtime Polymorphism
    @Override
    public String toString() {
        return super.toString()
            + "\nVolksWagen --> " + "aireAcondicionado=" + aireAcondicionado;
    }
}
