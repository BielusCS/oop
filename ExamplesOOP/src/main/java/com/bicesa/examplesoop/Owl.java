/*
 * Abstraction - Implementation
 */
package com.bicesa.examplesoop;

// Subclass that inherits from AnimalAbstract
public class Owl extends AnimalAbstract {

    @Override
    public void animalSound() {
        // The body of animalSound() is provided here
        System.out.println("The owl says: uhuuu uhuuu");
    }

    @Override
    public void animalMovement() {
        // The body of animalMovement() is provided here
        System.out.println("The owl moves: with its wings");
    }    
}
