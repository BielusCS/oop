/*
 * Abstraction - Interface Implementation
 */
package com.bicesa.examplesoop;

// Class that implements AnimalInterface
public class Animal2 implements AnimalInterface {

    @Override
    public void animalSound() {
        // The body of animalSound() is provided here
        System.out.println("The animal2 says: miau miau");
    }

    @Override
    public void animalMovement() {
        // The body of animalMovement() is provided here
        System.out.println("The animal2 moves: with its paws");
    }   
}
