/*
 * Multilevel Inheritance - Child Class
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public class PajaroMultilevel extends PajaroSingle {

    // Properties - Encapsulation
    private Boolean conChip;
    private Boolean conAnilla;    

    // Constructor
    public PajaroMultilevel(String lugarNacimiento, String originarioDe, String tamanio, String[] colores, int edad, float peso, Boolean esHembra, Boolean domesticado, String nombre, String caracter, Boolean conChip, Boolean conAnilla) {
        super(lugarNacimiento, originarioDe, tamanio, colores, edad, peso, esHembra, domesticado, nombre, caracter);
        this.conChip = conChip;
        this.conAnilla = conAnilla;
    }

    // Getters and Setters - Encapsulation
    public Boolean getConChip() {
        return conChip;
    }

    public void setConChip(Boolean conChip) {
        this.conChip = conChip;
    }

    public Boolean getConAnilla() {
        return conAnilla;
    }

    public void setConAnilla(Boolean conAnilla) {
        this.conAnilla = conAnilla;
    }

    // Method Overriding - Runtime Polymorphism
    @Override
    public String toString() {
        return super.toString()
            + "\nPajaroMultiLevel --> " + "conChip=" + getConChip() + ", conAnilla=" + getConAnilla();
    }
}
