/*
 * Inheritance - Parent Class
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public class Pajaro {

    // Properties - Encapsulation
    private String lugarNacimiento;
    private String originarioDe;
    private String tamanio;
    private String[] colores;
    private int edad;
    private float peso;
    private Boolean esHembra;
    private Boolean domesticado;

    // Constructor
    public Pajaro(String lugarNacimiento, String originarioDe, String tamanio, String[] colores, int edad, float peso, Boolean esHembra, Boolean domesticado) {
        this.lugarNacimiento = lugarNacimiento;
        this.originarioDe = originarioDe;
        this.tamanio = tamanio;
        this.colores = colores;
        this.edad = edad;
        this.peso = peso;
        this.esHembra = esHembra;
        this.domesticado = domesticado;
    }

    // Method Overloading - Compile-time Polymorphism
    public Pajaro(String lugarNacimiento, String tamanio, int edad, float peso, Boolean esHembra) {
        this.lugarNacimiento = lugarNacimiento;
        this.originarioDe = "sin especificar";
        this.tamanio = tamanio;
        this.colores = null;
        this.edad = edad;
        this.peso = peso;
        this.esHembra = esHembra;
        this.domesticado = null;
    }

    // Getters and Setters - Encapsulation
    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public String getOriginarioDe() {
        return originarioDe;
    }

    public void setOriginarioDe(String originarioDe) {
        this.originarioDe = originarioDe;
    }

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    public String[] getColores() {
        return colores;
    }

    public void setColores(String[] colores) {
        this.colores = colores;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public Boolean getEsHembra() {
        return esHembra;
    }

    public void setEsHembra(Boolean esHembra) {
        this.esHembra = esHembra;
    }

    public Boolean getDomesticado() {
        return domesticado;
    }

    public void setDomesticado(Boolean domesticado) {
        this.domesticado = domesticado;
    }

    // Methods
    public void volar() {
        System.out.println("El pájaro vuela.");
    }
    
    public void caminar() {
        System.out.println("El pájaro camina.");
    }
    
    public void beber() {
        System.out.println("El pájaro bebe.");
    }
    
    public void comer() {
        System.out.println("El pájaro come.");
    }
    
    public void dormir() {
        System.out.println("El pájaro duerme.");
    }
    
    private String insertarColores(String infoPajaro) {
        for (int i=0; i<colores.length; i++) {            
            if (i == colores.length-1)
                infoPajaro += colores[i] + "}, ";
            else
                infoPajaro += colores[i] + ", ";
        }
        
        return infoPajaro;
    }

    // Method Overriding - Runtime Polymorphism
    @Override
    public String toString() {
        String infoPajaro = "Pajaro --> " + "lugarNacimiento=" + lugarNacimiento + ", originarioDe=" + originarioDe
                + ", tamanio=" + tamanio + ", colores={";
        
        infoPajaro = insertarColores(infoPajaro);
        
        return infoPajaro + "edad=" + edad + ", peso=" + peso
            + ", esHembra=" + esHembra + ", domesticado=" + domesticado;
    }
}
