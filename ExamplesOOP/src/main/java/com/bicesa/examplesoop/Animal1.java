/*
 * Abstraction - Interface Implementation
 */
package com.bicesa.examplesoop;

// Class that implements AnimalInterface
public class Animal1 implements AnimalInterface {

    @Override
    public void animalSound() {
        // The body of animalSound() is provided here
        System.out.println("The animal1 says: guau guau");
    }

    @Override
    public void animalMovement() {
        // The body of animalMovement() is provided here
        System.out.println("The animal1 moves: with its paws");
    }   
}
