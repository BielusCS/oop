/*
 * Multiple Inheritance - Implementation
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public class MI_Impl implements MI_A, MI_B {
    
    @Override
    public void show() {
        // Use of 'super' keyword to call the 'show()' method of MI_A interface
        MI_A.super.show();
  
        // Use of 'super' keyword to call the 'show()' method of MI_B interface
        MI_B.super.show();
    }
}
