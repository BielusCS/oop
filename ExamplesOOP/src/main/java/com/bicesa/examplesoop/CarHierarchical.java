/*
 * Inheritance - Parent Class
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public class CarHierarchical {
    
    // Properties - Encapsulation
    private String marca;
    private String modelo;
    private int anioLanzamiento;
    private int potencia;
    private int puertas;

    // Constructor
    public CarHierarchical(String marca, String modelo, int anioLanzamiento, int potencia, int puertas) {
        this.marca = marca;
        this.modelo = modelo;
        this.anioLanzamiento = anioLanzamiento;
        this.potencia = potencia;
        this.puertas = puertas;
    }
    
    // Getters and Setters - Encapsulation
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAnioLanzamiento() {
        return anioLanzamiento;
    }

    public void setAnioLanzamiento(int anioLanzamiento) {
        this.anioLanzamiento = anioLanzamiento;
    }

    public int getPotencia() {
        return potencia;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    public int getPuertas() {
        return puertas;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }

    // Methods
    public void arrancar() {
        System.out.println("El coche ha arrancado.");
    }
    
    public void mover() {
        System.out.println("El coche se está moviendo.");
    }
    
    public void frenar() {
        System.out.println("El coche está frenando.");
    }
    
    public void parar() {
        System.out.println("El coche ha parado.");
    }
    
    // Method Overriding - Runtime Polymorphism
    @Override
    public String toString() {
        return "CarHierarchical --> " + "marca=" + marca + ", modelo=" + modelo
            + ", anioLanzamiento=" + anioLanzamiento + ", potencia=" + potencia + ", puertas=" + puertas;
    } 
}
