/*
 * Various examples of:
 *  - Inheritance
 *  - Encapsulation
 *  - Abstraction
 *  - Polymorphism
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public class MainProgram {
    
    public static void main(String[] args) {
        
        // Inheritance - Parent Class
        String[] colors = {"verde", "gris"};
        Pajaro p1 = new Pajaro(
            "Bcn", "Argentina", "M", colors, 1, 78.4f, true, true
        );
        System.out.println(">> Inheritance - Parent Class <<\n");
        System.out.println(p1.toString());
        p1.caminar();
        p1.dormir();
        
        // Single Inheritance - Child Class
        String[] colors2 = {"verde", "gris", "amarillo", "naranja"};
        PajaroSingle kiwi = new PajaroSingle(
            "Holanda", "Senegal", "M", colors2, 13, 122.1f, true, false, "Kiwi", "Malo"
        );
        System.out.println("\n\n>> Single Inheritance - Child Class <<\n");
        System.out.println(kiwi.toString());
        kiwi.hablar();
        kiwi.jugar();
        
        // Multilevel Inheritance - Child Class
        String[] colors3 = {"verde, amarillo"};
        PajaroMultilevel p3 = new PajaroMultilevel(
            "Bcn", "Australia", "S", colors3, 1, 62.7f, false, true, "Bitxo", "Bueno", false, true
        );
        System.out.println("\n\n>> Multilevel Inheritance - Child Class <<\n");
        System.out.println(p3.toString());
        p3.beber();
        p3.volar();
        
        // Hierarchical Inheritance - Parent Class
        CarHierarchical car = new CarHierarchical("Mercedez", "Smart", 2005, 70, 3);
        System.out.println("\n\n>> Hierarchical Inheritance - Parent Class <<\n");
        System.out.println(car.toString());
        car.arrancar();
        car.parar();
        
        // Hierarchical Inheritance - Child Classes
        VolksWagen vw = new VolksWagen("Volkswagen", "Polo", 1990, 75, 5, true);
        System.out.println("\n\n>> Hierarchical Inheritance - Child Class <<\n");
        System.out.println(vw.toString());
        vw.arrancar();
        vw.mover();
        
        Audi au = new Audi("Audi", "A3", 1994, 110, 3, true, true);
        System.out.println("\n\n>> Hierarchical Inheritance - Child Class <<\n");
        System.out.println(au.toString());
        au.frenar();
        au.parar();
        
        // Multiple Inheritance
        MI_Impl mi = new MI_Impl();
        System.out.println("\n\n>> Multiple Inheritance <<\n");
        mi.show();
        
        // Abstraction - Abstract class, abstract methods and regular methods
        Owl myOwl = new Owl();
        System.out.println("\n\n>> Abstraction - Abstract Class <<\n");
        myOwl.animalSound();
        myOwl.animalMovement();
        myOwl.eat();
        myOwl.sleep();
        
        // Abstraction - Interface and interface methods
        Animal1 a1 = new Animal1();
        System.out.println("\n\n>> Abstraction - Interface Implementation #1 <<\n");
        a1.animalMovement();
        a1.animalSound();
        
        Animal2 a2 = new Animal2();
        System.out.println("\n\n>> Abstraction - Interface Implementation #2 <<\n");
        a2.animalMovement();
        a2.animalSound();
    }
}
