/*
 * Hierarchical Inheritance - Child Class
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public class Audi extends CarHierarchical {

    // Properties - Encapsulation
    private Boolean ordenador;
    private Boolean direccionAsistida;

    // Constructor
    public Audi(String marca, String modelo, int anioLanzamiento, int potencia, int puertas, Boolean ordenador, Boolean direccionAsistida) {
        super(marca, modelo, anioLanzamiento, potencia, puertas);
        this.ordenador = ordenador;
        this.direccionAsistida = direccionAsistida;
    }

    // Getters and Setters - Encapsulation
    public Boolean getOrdenador() {
        return ordenador;
    }

    public void setOrdenador(Boolean ordenador) {
        this.ordenador = ordenador;
    }

    public Boolean getDireccionAsistida() {
        return direccionAsistida;
    }

    public void setDireccionAsistida(Boolean direccionAsistida) {
        this.direccionAsistida = direccionAsistida;
    }

    // Method Overriding - Runtime Polymorphism
    @Override
    public String toString() {
        return super.toString()
            + "\nAudi --> " + "ordenador=" + ordenador + ", direccionAsistida=" + direccionAsistida;
    }
}
