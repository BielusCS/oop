/*
 * Abstraction - Interface and interface methods
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public interface AnimalInterface {
    // Interface methods (don't have a body)
    public void animalSound();
    public void animalMovement();
}
