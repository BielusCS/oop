/*
 * Multiple Inheritance - Default methods
 */
package com.bicesa.examplesoop;

/**
 *
 * @author usuari
 */
public interface MI_B {
    // Default method
    default void show() {
        System.out.println("Default Method --> Multiple Inheritance B");
    }
}
